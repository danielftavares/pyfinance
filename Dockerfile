FROM python:3.7-alpine
ENV PYTHONUNBUFFERED 1

RUN apk update
RUN apk add dcron musl-dev wget git build-base postgresql-dev \
        freetype \
        linux-headers \
        gcc \
        g++ \
        make \
        libpng \
        libjpeg-turbo \
        freetype-dev \
        libpng-dev \
        jpeg-dev \
        libjpeg \
        libjpeg-turbo-dev \
        tzdata

RUN  cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime && \
    echo "America/Sao_Paulo" > /etc/timezone && \
    apk del tzdata && rm -rf /var/cache/apk/*


RUN wget http://prdownloads.sourceforge.net/ta-lib/ta-lib-0.4.0-src.tar.gz && \
  tar -xvzf ta-lib-0.4.0-src.tar.gz && \
  cd ta-lib/ && \
  ./configure --prefix=/usr && \
  make && \
  make install

RUN pip install cython
RUN ln -s /usr/include/locale.h /usr/include/xlocale.h



RUN mkdir /pyFinance
WORKDIR /pyFinance

COPY ./requirements.txt  ./
COPY ./setup.py  ./
RUN pip install -r requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/pyFinance"

RUN echo "*/5 * * * * /pyFinance/bash/batch_import.sh  >> /pyFinance/bash/crondocker.log" >> /etc/crontabs/root

COPY entrypoint /entrypoint
RUN chmod +x /entrypoint
ENTRYPOINT ["/entrypoint"]

CMD tail -f /dev/null
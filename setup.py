from setuptools import setup

setup(
    name='pyFinance',
    version='',
    packages=['pyfinance'],
    url='',
    license='',
    author='danielfingertavares',
    author_email='',
    description='', install_requires=['requests', 'psycopg2-binary', 'TA-Lib', 'matplotlib']
)

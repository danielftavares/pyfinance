from enum import Enum
import datetime


class TimeFrame(Enum):
    V15m = {"time": "15m", "delta": datetime.timedelta(minutes=15)}
    V30m = {"time": "30m", "delta": datetime.timedelta(minutes=30)}
    V1h = {"time": "1h", "delta": datetime.timedelta(hours=1)}
    V6h = {"time": "6h", "delta": datetime.timedelta(hours=6)}
    V12h = {"time": "12h", "delta": datetime.timedelta(hours=12)}
    V1D = {"time": "1D", "delta": datetime.timedelta(days=1)}


class Symbol(Enum):
    ETHUSD = "ETHUSD"


class Candle:

    def __init__(self, time, open, close, high, low, volume, symbol, time_frame):
        self.time_frame = time_frame
        self.symbol = symbol
        self.time = time
        self.open = open
        self.close = close
        self.high = high
        self.low = low
        self.volume = volume

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "Candle(time_frame={self.time_frame}, symbol={self.symbol}, time={self.time}, open={self.open}, " \
               "close={self.close}, high={self.high}, low={self.low}, volume={self.volume})".format(self=self)

    def __eq__(self, other):
        return self.time_frame == other.time_frame and \
               self.symbol == other.symbol and \
               self.time == other.time

    def __hash__(self):
        return hash((self.time_frame, self.symbol, self.time))


def msToTime(ms):
    try:
        return datetime.datetime.fromtimestamp(ms / 1000)
    except TypeError as e:
        print(ms)
        print(e)
        raise


def timeToMs(time):
    try:
        return time.timestamp() * 1000
    except TypeError as e:
        print(time)
        print(e)
        raise

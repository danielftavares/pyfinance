import requests
import datetime
from . import TimeFrame, timeToMs, Symbol, msToTime, Candle


class BitfinexClient:
    URL = "https://api.bitfinex.com"

    def loadCandle(self, pair=Symbol.ETHUSD, time_frame=TimeFrame.V15m, start=None):
        if start is not None:
            td_now = self.get_remote_datetime()
            if td_now is None:
                return []
            arrayreturn = []
            workstart = start
            while True:
                workend = workstart + time_frame.value["delta"] * 100
                if workend >= td_now:
                    workend = None
                    if (td_now - workstart)/time_frame.value["delta"] <= 100:
                        print("carregando os ultimos 50")
                        workstart = td_now - (time_frame.value["delta"] * 100)
                lastreturn=self.__get_candles__request__(pair, time_frame, workstart, workend)
                print(len(lastreturn))
                arrayreturn.extend(lastreturn)
                if len(lastreturn) == 0 or workend is None:
                    return arrayreturn
                workstart = workend + time_frame.value["delta"]
                if workstart > td_now:
                    return lastreturn
        else:
            return self.__get_candles__request__(pair, time_frame)

    def __get_candles__request__(self, symbol=Symbol.ETHUSD, timeFrame=TimeFrame.V15m, start=None, end=None):
        urlrequest=BitfinexClient.URL + "/v2/candles/trade:" + timeFrame.value["time"] + ":t" + symbol.value + "/hist"
        parameters = {}
        if start is not None:
            parameters["start"] = timeToMs(start)
        if end is not None:
            parameters["end"] = timeToMs(end)
        r = requests.get(urlrequest, parameters)

        if r.status_code == 200:
            return [self.__candleToObj__(c, symbol, timeFrame) for c in r.json()]
        else:
            print(r)
            print(r.content)
            return []

    @staticmethod
    def get_remote_datetime():
        url_request = BitfinexClient.URL + "/v2/candles/trade:1m:tBTCUSD/last"
        r = requests.get(url_request)
        if r.status_code == 200:
            return msToTime(r.json()[0])
        else:
            print(r)
            print(r.content)
            return None

    @staticmethod
    def get_ticker(symbol = Symbol.ETHUSD):
        url_request = BitfinexClient.URL + "/v2/ticker/t" + symbol.value
        r = requests.get(url_request)
        if r.status_code == 200:
            ticker = r.json()
            return {
                "BID": ticker[0],
                "BID_SIZE": ticker[1],
                "ASK": ticker[2],
                "ASK_SIZE": ticker[3],
                "DAILY_CHANGE": ticker[4],
                "DAILY_CHANGE_PERC": ticker[5],
                "LAST_PRICE": ticker[6],
                "VOLUME": ticker[7],
                "HIGH": ticker[8],
                "LOW": ticker[9]
            }
        else:
            print(r)
            print(r.content)
            return None

    @staticmethod
    def __candleToObj__(candle_array, symbol, time_frame):
        return Candle(time=msToTime(candle_array[0]),
                      open=candle_array[1],
                      close=candle_array[2],
                      high=candle_array[3],
                      low=candle_array[4],
                      volume=candle_array[5],
                      symbol=symbol.value,
                      time_frame=time_frame.value["time"])



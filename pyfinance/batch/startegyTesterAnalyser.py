from pyfinance.database.operationdb import OperationDAO
from pyfinance.strategies.wallet import WalletTestStrategy, TestWallet
from pyfinance.strategies import Operation

op_dao = OperationDAO()
lo = op_dao.listOperationsTest("primeiro teste")

wallet = TestWallet(coin=0.3, usd=0)
walletStrategy = WalletTestStrategy(wallet = wallet)

for operation in lo:
    if operation.operation == Operation.OPERATION_BUY:
        walletStrategy.buy(operation.value)
    elif operation.operation == Operation.OPERATION_SELL:
        walletStrategy.sell(operation.value)


print(wallet)
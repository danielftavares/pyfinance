from pyfinance.strategies.strateg_runner import run_strategy
from pyfinance.strategies.macdstrategy import MACDStrategy
from pyfinance.exchange import TimeFrame, Symbol
import datetime

dt_start = datetime.datetime(year=2019, month=9, day=1)
macdstrategy = MACDStrategy()

while dt_start < datetime.datetime.now():
    run_strategy(strategy=macdstrategy,
                 date_time=dt_start,
                 symbol=Symbol.ETHUSD,
                 id_test="mad112h")
    dt_start = dt_start + TimeFrame.V12h.value["delta"]

from pyfinance.exchange.bitfinex import BitfinexClient, TimeFrame
from pyfinance.database.candledb import CandleDAO
import datetime
import logging




def importCandles(time_frame=TimeFrame.V15m):
    str_time_frame = time_frame.value["time"]
    logger = logging.getLogger('batch_'+str_time_frame)
    logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler('batch_'+str_time_frame+'.log')
    fh.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    logger.info("Importanto candles")

    dao = CandleDAO()
    client = BitfinexClient()
    last_dt_imported = dao.last_candle_imported(time_frame)
    if last_dt_imported is None:
        last_dt_imported = datetime.datetime(year=2019, month=1, day=1, hour=1)
    else:
        last_dt_imported = last_dt_imported - (3 * time_frame.value["delta"])
    candles = client.loadCandle(time_frame=time_frame, start=last_dt_imported)
    logger.info("candles importadas:")
    logger.info(candles)
    logger.info("inserindo na base de dados")
    dao.insert_list(candles)
    logger.info("fim importacao")

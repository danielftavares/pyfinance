from pyfinance.batch.bitfiexetl import importCandles
from pyfinance.exchange import TimeFrame


for time_frame in TimeFrame:
    importCandles(time_frame=time_frame)
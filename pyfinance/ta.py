import talib
import numpy
from pyfinance.database.candledb import CandleDAO
import matplotlib.pyplot as plt
from pyfinance.exchange import TimeFrame

dao = CandleDAO()

candles = dao.load_last_candles(1000, time_frame=TimeFrame.V30m )


inputs = {
    'open': numpy.array([candle.open for candle in candles]),
    'high': numpy.array([candle.high for candle in candles]),
    'low': numpy.array([candle.low for candle in candles]),
    'close': numpy.array([candle.close for candle in candles]),
    'volume': numpy.array([candle.volume for candle in candles])
}

close = numpy.array([candle.close for candle in candles])
time = numpy.array([candle.time for candle in candles])


macdta, macdsignalta, macdhistta = talib.MACD(close, fastperiod=12, slowperiod=26, signalperiod=9)

#real = talib.DEMA(inputs["close"], timeperiod=re)


fastLength = 12
slowlength = 26
MACDLength = 9

MACD = talib.EMA(close, timeperiod=fastLength) - talib.EMA(close, timeperiod=slowlength)
aMACD = talib.EMA(MACD, MACDLength)
delta = MACD - aMACD


print(close[-5:])
print(MACD[-5:])
print(macdta[-5:])

plt.plot(time, delta, label='open')
plt.legend()

#print(macdsignal[-5:])


#print(inputs["open"] - inputs["close"])

#fig, axes = plt.subplots(nrows=2, ncols=1, sharex=True)


#axes[0].plot(time, inputs["open"], label='open')
#axes[0].plot(time, inputs["close"], label='close')
#axes[0].legend()

#axes[1].plot(time, macd, label='MACD')
#axes[1].plot(time, macdsignal, label='MACD Signal')
#axes[1].plot(time, macdhist, label='MACD Hist.')
#axes[1].legend()

plt.grid()

plt.show()

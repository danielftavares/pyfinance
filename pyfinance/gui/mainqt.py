import sys

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import Qt
from pyfinance.gui.plot.macd import MACDCanvas


class DTAApp(QMainWindow):

    def __init__(self):
        super().__init__()
        self.left = 10
        self.top = 10
        self.title = 'DTAApp'
        self.width = 640
        self.height = 400
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.statusBar().showMessage('Ready')

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('File')

        impMenu = QMenu('Import', self)
        impAct = QAction('Import mail', self)
        impMenu.addAction(impAct)

        newAct = QAction('New', self)

        fileMenu.addAction(newAct)
        fileMenu.addMenu(impMenu)

        exitAct = QAction(QIcon('exit24.png'), 'Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.close)

        toolbar = self.addToolBar('Exit')
        toolbar.addAction(exitAct)
        toolbar.setMovable(False);

        myToolbar = QToolBar()
        self.addToolBar(Qt.LeftToolBarArea, myToolbar)

        oTabWidget = QTabWidget()
        oPage1 = MACDCanvas()
        oPage2 = QWidget()
        oPage3 = QWidget()
        oTabWidget.addTab(oPage1, "Page1")
        oTabWidget.addTab(oPage2, "Page2")
        oTabWidget.addTab(oPage3, "Page3")

        layout = QVBoxLayout(self)
        layout.addWidget(oTabWidget)

        self.setCentralWidget(oTabWidget)

        self.show()




if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = DTAApp()
    sys.exit(app.exec_())
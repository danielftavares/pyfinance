
import talib
import numpy
from pyfinance.database.candledb import CandleDAO
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.figure import Figure
from pyfinance.gui import MyMplCanvas

from pyfinance.exchange import TimeFrame


def getMACDPlot():
    dao = CandleDAO()

    candles = dao.load_last_candles(1000, time_frame=TimeFrame.V30m )


    inputs = {
        'open': numpy.array([candle.open for candle in candles]),
        'high': numpy.array([candle.high for candle in candles]),
        'low': numpy.array([candle.low for candle in candles]),
        'close': numpy.array([candle.close for candle in candles]),
        'volume': numpy.array([candle.volume for candle in candles])
    }

    close = numpy.array([candle.close for candle in candles])
    time = numpy.array([candle.time for candle in candles])


    macdta, macdsignalta, macdhistta = talib.MACD(close, fastperiod=12, slowperiod=26, signalperiod=9)

    f = Figure(figsize=(5, 5), dpi=100)
    a = f.add_subplot(111)
    a.plot(time, macdta, label='open')

    return f






class MACDCanvas(MyMplCanvas):
    """Simple canvas with a sine plot."""

    def compute_initial_figure(self):
        dao = CandleDAO()

        candles = dao.load_last_candles(1000, time_frame=TimeFrame.V30m)

        inputs = {
            'open': numpy.array([candle.open for candle in candles]),
            'high': numpy.array([candle.high for candle in candles]),
            'low': numpy.array([candle.low for candle in candles]),
            'close': numpy.array([candle.close for candle in candles]),
            'volume': numpy.array([candle.volume for candle in candles])
        }

        close = numpy.array([candle.close for candle in candles])
        time = numpy.array([candle.time for candle in candles])

        macdta, macdsignalta, macdhistta = talib.MACD(close, fastperiod=12, slowperiod=26, signalperiod=9)

        self.axes.plot(time, macdta, label='open')
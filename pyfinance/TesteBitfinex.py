import datetime
from pyfinance.exchange.bitfinex import BitfinexClient, Symbol
from pyfinance.database import OperationDAO
from pyfinance.strategies import Operation
from pyfinance.strategies.macdstrategy import MACDStrategy

# millis = int(round(time.time() * 1000))

client = BitfinexClient()

ticker = client.get_ticker(Symbol.ETHUSD)

dao = OperationDAO()

op = Operation(operation_datetime=datetime.datetime.now(),
               operation=Operation.OPERATION_BUY,
               strategy=MACDStrategy(),
               amount=0,
               symbol=Symbol.ETHUSD,
               value=ticker["ASK"])

dao.insert(operation=op)

# candles = client.loadCandle(start=datetime.datetime(year=2018, month=5, day=1))
# print(candles)
# print(len(candles))

# dao = CandleDAO()
# dao.insert_list(candles)

# print(client.get_remote_datetime())
# print(dao.last_candle_imported())

# print(dao.load_last_candles())

# print(len(loadCandle(start=datetime.datetime(year=2018, month=5, day=1))))
# print(timeToMs(msToTime(1527427800000)))
# print(msToTime(1527427800000))

import abc
from pyfinance.database.candledb import CandleDAO
import numpy
from pyfinance.exchange import TimeFrame, Symbol
from pyfinance.exchange.bitfinex import BitfinexClient


def get_close(input):
    return numpy.array([close for close in input['close']])


def crossed_up(first, second):
    if not isinstance(second, list):
        second = [second] * len(first)

    if not isinstance(first, list):
        first = [first] * len(second)

    had_down = False
    for i in range(len(first)):
        f = first[i]
        s = second[i]
        is_down = f < s
        if is_down:
            had_down = is_down
        elif had_down:
            return True
    return False


def crossed_down(first, second):
    return crossed_up(second, first)


class Operation:
    OPERATION_BUY = "BUY"
    OPERATION_NEUTRAL = "NEUTRAL"
    OPERATION_SELL = "SELL"

    def __init__(self, operation_datetime=BitfinexClient.get_remote_datetime(), operation=OPERATION_NEUTRAL,
                 strategy="", amount=0, symbol=Symbol.ETHUSD, value=0, id_test=None):
        self.id_test = id_test
        self.symbol = symbol.value
        self.amount = amount
        self.strategy = strategy.__class__.__name__
        self.operation_datetime = operation_datetime
        self.value = value
        self.operation = operation


class Strategy(abc.ABC):
    @abc.abstractmethod
    def get_operation(self, symbol=Symbol.ETHUSD, date_time=None):
        return

    def get_candle_dao(self):
        return CandleDAO()

    def load_candles_input(self, num_candles=100, time_frame=TimeFrame.V15m, symbol=Symbol.ETHUSD, date_time=None):
        dao = self.get_candle_dao()
        candles = dao.load_last_candles(num_candles=num_candles, time_frame=time_frame, symbol=symbol, date_time=date_time)
        inputs = {
            'open': numpy.array([candle.open for candle in candles]),
            'high': numpy.array([candle.high for candle in candles]),
            'low': numpy.array([candle.low for candle in candles]),
            'close': numpy.array([candle.close for candle in candles]),
            'volume': numpy.array([candle.volume for candle in candles])
        }
        return inputs



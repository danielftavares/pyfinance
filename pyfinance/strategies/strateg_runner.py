from pyfinance.database.operationdb import OperationDAO
from pyfinance.exchange import Symbol
from pyfinance.exchange.bitfinex import BitfinexClient
from pyfinance.strategies import Strategy, Operation


def run_strategy(strategy: Strategy, symbol=Symbol.ETHUSD, date_time=None, id_test=None):
    tp_operation, price_sgt = strategy.get_operation(symbol=symbol, date_time=date_time)
    if tp_operation != Operation.OPERATION_NEUTRAL:
        client = BitfinexClient()
        if id_test is None:
            ticker = client.get_ticker(Symbol.ETHUSD)
            price_sgt = ticker["ASK"]
        operation = Operation(operation_datetime=date_time,
                              operation=tp_operation,
                              strategy=strategy.__class__.__name__,
                              value=price_sgt,
                              amount=0,  # TODO
                              id_test=id_test)
        dao = OperationDAO()
        dao.insert(operation)
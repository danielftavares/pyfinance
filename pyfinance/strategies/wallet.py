
class TestWallet:

    def __init__(self, coin, usd):
        self.usd = usd
        self.coin = coin

    def buy(self, value, amount):
        self.coin = self.coin + amount
        self.usd = self.usd - (value * amount)

    def sell(self, value, amount):
        self.coin = self.coin - amount
        self.usd = self.usd + (value * amount)

    def __str__(self):
        return "Wallet(coin={self.coin}, usd={self.usd})".format(self=self)


class WalletTestStrategy:
    MIN_AMOUNT = 0.01

    def __init__(self, wallet):
        self.wallet = wallet

    def can_i_buy(self, value):
        if ((self.wallet.usd / 2) / value) > WalletTestStrategy.MIN_AMOUNT:
            return True
        else:
            return False

    def can_i_sell(self, value):
        if (self.wallet.coin / 2) > WalletTestStrategy.MIN_AMOUNT:
            return True
        else:
            return False

    def buy(self, value):
        amount = (self.wallet.usd / 2) / value
        if self.can_i_buy(value):
            self.wallet.buy(value, amount)
            return True
        return False

    def sell(self, value):
        amount = self.wallet.coin / 2
        if self.can_i_sell(value):
            self.wallet.sell(value, amount)
            return True
        return False
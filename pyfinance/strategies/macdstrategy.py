from . import Strategy, get_close, crossed_down, crossed_up, Operation
from pyfinance.exchange import TimeFrame, Symbol
import talib


class MACDStrategy(Strategy):

    def get_operation(self, symbol=Symbol.ETHUSD, date_time=None):
        input = self.load_candles_input(num_candles=100, time_frame=TimeFrame.V12h, symbol=symbol, date_time=date_time)
        macd, macdsignal, macdhist = talib.MACD(get_close(input), fastperiod=12, slowperiod=26, signalperiod=9)
        if crossed_up(macdhist[-2:].tolist(), 0):
            return Operation.OPERATION_BUY, get_close(input)[-1]

        if crossed_down(macdhist[-2:].tolist(), 0):
            return Operation.OPERATION_SELL, get_close(input)[-1]

        return Operation.OPERATION_NEUTRAL, None

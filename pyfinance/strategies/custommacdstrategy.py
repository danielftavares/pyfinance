from . import Strategy, get_close, crossed_down, crossed_up, Operation
from pyfinance.exchange import TimeFrame, Symbol
import talib


class MACDStrategy(Strategy):

    def get_operation(self, symbol=Symbol.ETHUSD, date_time=None):
        candles1H = self.load_candles_input(num_candles=100, time_frame=TimeFrame.V1h, symbol=symbol, date_time=date_time)

        candles15m = self.load_candles_input(num_candles=100, time_frame=TimeFrame.V15m, symbol=symbol, date_time=date_time)
        macd15m, macdsignal15m, macdhist15m = talib.MACD(get_close(candles15m), fastperiod=12, slowperiod=26, signalperiod=9)
        macd1h, macdsignal1h, macdhist1h = talib.MACD(get_close(candles1H), fastperiod=12, slowperiod=26, signalperiod=9)

        if macd1h[-1] > 0:
            #tendendia no macd de uma hora é de alta
            if crossed_up(macd15m[-2:], macd1h[-2:]):
                #tendencia de alta
                return Operation.OPERATION_BUY, get_close(input)[-1]
            if crossed_up(macd1h[-2:], 0) and macd15m[-1] > 0:
                return Operation.OPERATION_BUY, get_close(input)[-1]
            if crossed_down(macd1h)
        else:



        if crossed_up(macd1h[-2].tolist(), 0):
            return Operation.OPERATION_BUY, get_close(input)[-1]

        if crossed_down(macd1h[-3].tolist(), 0):
            return Operation.OPERATION_SELL, get_close(input)[-1]

        return Operation.OPERATION_NEUTRAL, None

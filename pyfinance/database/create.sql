CREATE USER finance WITH PASSWORD 'financepass';


CREATE DATABASE financedb
    WITH
    OWNER = finance
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

CREATE TABLE candle
(
    id integer PRIMARY KEY,
    symbol varchar(7) NOT NULL,
    time_frame varchar(3) NOT NULL,
	candle_datetime timestamp not null,
	open numeric(12,6) not null,
	close numeric(12,6) not null,
	high numeric(12,6) not null,
	low numeric(12,6) not null,
	volume  numeric(12,6) not null

);


CREATE SEQUENCE seq_candle;


CREATE TABLE operation
(
    id integer PRIMARY KEY,
    operation_datetime timestamp not null,
    operation varchar(10) NOT NULL,
    strategy varchar(50) NOT NULL,
    value numeric(12,6) not null,
    symbol varchar(7) NOT NULL,
    amount numeric(12,6) not null,
    id_test varchar(50) NOT NULL

);


CREATE SEQUENCE seq_operation;
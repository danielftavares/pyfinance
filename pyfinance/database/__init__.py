import psycopg2


class GernericDao:
    CONNECTION_STR = "host=postgres_container dbname=financedb user=finance  password=financepass"

    def open_connection(self):
        return psycopg2.connect(GernericDao.CONNECTION_STR)

    def close_connection(self, conn, cur):
        cur.close()
        conn.close()
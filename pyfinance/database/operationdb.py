from . import GernericDao
import psycopg2.extras
from pyfinance.exchange import Symbol
from pyfinance.strategies import Operation


class OperationDAO(GernericDao):


    def insert(self, operation):
        conn = self.open_connection()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("""INSERT INTO operation(id, operation_datetime, operation, strategy, value, symbol, amount, id_test) 
                VALUES (nextval('seq_operation'), 
                        %(operation_datetime)s, 
                        %(operation)s, 
                        %(strategy)s, 
                        %(value)s, 
                        %(symbol)s, 
                        %(amount)s,
                        %(id_test)s);"""
                    , vars(operation))
        conn.commit()
        self.close_connection(conn, cur)

    def listOperationsTest(self, id_test):

        conn = self.open_connection()
        cur = conn.cursor()

        sql = """SELECT
                    id, 
                    operation_datetime,
                    operation, 
                    strategy, 
                    symbol, 
                    amount,
                    value, 
                    id_test
                FROM operation
                WHERE id_test = %(id_test)s
                ORDER BY operation_datetime ASC
                """

        cur.execute(sql, {"id_test": id_test})
        x = cur.fetchall()
        self.close_connection(conn, cur)
        candles = [self.__row_to_operation__(row) for row in x]
        candles.reverse()
        return candles

    def __row_to_operation__(self, row):
        return Operation(operation_datetime=row[1],
                         operation=row[2],
                         strategy=row[3],
                         symbol=Symbol(row[4]),
                         amount=float(row[5]),
                         value=float(row[6]),
                         id_test=row[7])


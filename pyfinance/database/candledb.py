from . import GernericDao
import psycopg2.extras
from pyfinance.exchange import TimeFrame, Candle, Symbol


class CandleDAO(GernericDao):
    def insert_list(self, candle_array):
        conn = self.open_connection()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        for candle in candle_array:
            cur.execute("""SELECT id FROM CANDLE WHERE 
                        symbol=%(symbol)s AND time_frame=%(time_frame)s AND candle_datetime=%(time)s""", vars(candle))
            x = cur.fetchone()

            if x is None:
                cur.execute("""INSERT INTO candle(
                    id, symbol, time_frame, candle_datetime, open, close, high, low, volume)
                    VALUES (nextval('seq_candle'), %(symbol)s, %(time_frame)s, %(time)s, %(open)s,
                     %(close)s, %(high)s, %(low)s, %(volume)s)""", vars(candle))
            else:
                cur.execute("""update candle set open=%(open)s, close=%(close)s, 
                    high=%(high)s, low=%(low)s, volume=%(volume)s 
                    WHERE symbol=%(symbol)s AND time_frame=%(time_frame)s AND candle_datetime=%(time)s """,
                            vars(candle))
        conn.commit()
        self.close_connection(conn, cur)

    def last_candle_imported(self, time_frame=TimeFrame.V15m):
        conn = self.open_connection()
        cur = conn.cursor()
        cur.execute("select max(candle_datetime) from candle where time_frame = %(time_frame)s",
                    {"time_frame": time_frame.value["time"]})
        x = cur.fetchone()[0]
        self.close_connection(conn, cur)
        return x

    def load_last_candles(self, num_candles=1000, time_frame=TimeFrame.V15m, symbol=Symbol.ETHUSD, date_time=None):
        conn = self.open_connection()
        cur = conn.cursor()

        sql = """SELECT candle_datetime, open, close, high, low, volume 
                FROM candle where time_frame = %(time_frame)s and symbol = %(symbol)s """

        if date_time is not None:
            sql += " and candle_datetime < %(date_time)s "

        sql += " ORDER BY candle_datetime desc limit %(limit)s"

        cur.execute(sql, {"time_frame": time_frame.value["time"],
                          "symbol": symbol.value,
                          "limit": num_candles,
                          "date_time": date_time})
        x = cur.fetchall()
        self.close_connection(conn, cur)
        candles = [self.__row_to_candle__(row, symbol, time_frame) for row in x]
        candles.reverse()
        return candles

    @staticmethod
    def __row_to_candle__(row, symbol, time_frame):
        return Candle(time=row[0],
                      open=float(row[1]),
                      close=float(row[2]),
                      high=float(row[3]),
                      low=float(row[4]),
                      volume=float(row[5]),
                      symbol=symbol.value,
                      time_frame=time_frame.value["time"])

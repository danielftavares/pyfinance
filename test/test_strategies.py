import unittest
from pyfinance.strategies import *


class TestStrategies(unittest.TestCase):
    def test_crossed_up(self):
        first = [0, 10]
        secound = [5, 6]
        self.assertTrue(crossed_up(first, secound))

    def test_crossed_up_fixed_value(self):
        first = [0, 10]
        secound = 5
        self.assertTrue(crossed_up(first, secound))

    def test_crossed_up_not_valid(self):
        first = [0, 2]
        secound = [5, 5]
        self.assertFalse(crossed_up(first, secound))

    def test_crossed_up_not_valid_2(self):
        first = [7, 10]
        secound = [5, 5]
        self.assertFalse(crossed_up(first, secound))

    def test_crossed_up_not_valid_3(self):
        first = [7, 3]
        secound = [5, 5]
        self.assertFalse(crossed_up(first, secound))

    def test_crossed_down(self):
        first = [10, 0]
        secound = [5, 5]
        self.assertTrue(crossed_down(first, secound))

    def test_crossed_down_not_valid(self):
        first = [2, 0]
        secound = [5, 5]
        self.assertFalse(crossed_down(first, secound))

    def test_crossed_down_not_valid_2(self):
        first = [7, 8]
        secound = [5, 5]
        self.assertFalse(crossed_down(first, secound))

    def test_crossed_down_not_valid_3(self):
        first = [3, 8]
        secound = [5, 5]
        self.assertFalse(crossed_down(first, secound))
import unittest
from pyfinance.strategies.wallet import TestWallet

class TestStrategies(unittest.TestCase):
    def test_buy(self):
        w = TestWallet(usd=10, coin=10)

        w.buy(value=1, amount=1)

        self.assertEqual(w.coin, 11)
        self.assertEqual(w.usd, 9)

        w = TestWallet(usd=7, coin=10)
        w.buy(value=2, amount=3)
        self.assertEqual(w.coin, 13)
        self.assertEqual(w.usd, 1)

        w = TestWallet(usd=100, coin=0.1)
        w.buy(value=200, amount=0.5)
        self.assertEqual(w.coin, 0.6)
        self.assertEqual(w.usd, 0)


    def test_sell(self):
        w = TestWallet(usd=10, coin=10)

        w.sell(value=1, amount=1)

        self.assertEqual(w.coin, 9)
        self.assertEqual(w.usd, 11)

        w = TestWallet(usd=7, coin=10)
        w.sell(value=2, amount=3)
        self.assertEqual(w.coin, 7)
        self.assertEqual(w.usd, 13)

        w = TestWallet(usd=100, coin=0.6)
        w.sell(value=200, amount=0.5)
        self.assertEqual(w.coin, 0.1)
        self.assertEqual(w.usd, 200)